module Presentation {
  export class Boot extends Phaser.State {
    public preload() {
      // This needs to be loaded here as it is searched for images to preload
      // in the Preloader Class.
      this.load.text('slides', './assets/slides.json');

      // Load slide templates
      this.load.text('slide_templates', './assets/templates.json');
    }

    public create() {
      this.input.maxPointers = 1;
      this.stage.disableVisibilityChange = true;

      this.game.scale.pageAlignHorizontally = true;

      this.game.state.start('Preloader', true, false);
    }
  }
}

module Presentation {
  export class DialogueBoxLarge extends Phaser.Sprite {

    constructor(game:Phaser.Game) {
      super(game, 25, 600, 'dialogue_box_large', 0);
      this.animations.add('normal', [0], 0, false);

      // Large dialogue box is 550 x 140
      this.anchor.setTo(0, 1);
      this.visible = true;
      this.game.add.existing(this);
    }

    public play_animation(key:string):void {
      this.animations.play(key);
    }
  }

  export class DialogueBoxSmall extends Phaser.Sprite {

    constructor(game:Phaser.Game) {
      super(game, 890, 590, 'dialogue_box_small', 0);
      this.animations.add('normal', [0], 0, false);

      this.anchor.setTo(1, 1);
      this.visible = true;
      this.game.add.existing(this);
    }

    public play_animation(key:string):void {
      this.animations.play(key);
    }
  }
}

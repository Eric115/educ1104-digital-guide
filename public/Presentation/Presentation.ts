/// <reference path="../libs/phaser.d.ts"/>
/// <reference path="../libs/socket.io.d.ts"/>

module Presentation {
  export interface Slide {
    template: string;
    main_img?: string;
    host_speech?: string;
    host_animation: string;
  }

  export interface Template {
    background_img?: string;
    description: string;
    show_host?: boolean;
    use_large_host?: boolean;
    main_img_origin?: Phaser.Point;
    host_origin?: Phaser.Point;
    host_anchor?: Phaser.Point;
  }

  export class Main extends Phaser.Game {
    public static slideManager: SlideManager;
    public static connectionCode: number;
    public server: Server;
    public static self;

    constructor() {
      super(900, 600, Phaser.AUTO, 'container', null);

      Main.slideManager = new SlideManager();

      this.state.add('Boot', Boot, false);
      this.state.add('Preloader', Preloader, false);
      this.state.add('DigitalGuide', DigitalGuide, false);

      // Establish a connection to the server.
      this.server = new Server(this);

      this.state.start('Boot');
    }

    public next(): void {
      DigitalGuide.next();
    }

    public previous(): void {
      DigitalGuide.previous();
    }
  }
}

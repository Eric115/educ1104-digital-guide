module Presentation {
  export class HostSpeech extends Phaser.Text {
    private current_text: Array<string> = [];
    private current_text_count: number = -1;
    public style: Object;

    constructor(game: Phaser.Game) {
      super(game, 35, 470, '', {
        font: 'Arial',
        fontSize: '18px',
        fill: "#ffffff",
        wordWrap: true,
        wordWrapWidth: 830
      });

      this.game.add.existing(this);
    }

    /**
     * Breaks a string into chunks of text and set as current text.
     * Automatically displays first item.
     *
     * @param text The text to split and display.
     * @param size The host size 1 = small, 2 = large
     */
    public setCurrentSpeech(text: string, size: number): void {
      //default to small
      size = size || 1;
      // reset count.
      this.current_text_count = -1;
      // reverse array to that pop can be used later.
      if (size === 1) {
        this.current_text = text.replace(/.{325}\S*\s+/g, "$&@").split(/\s+@/);
      } else if (size == 2) {
        this.current_text = text.replace(/.{480}\S*\s+/g, "$&@").split(/\s+@/);
      }

      this.setNextText();
      this.visible = true;
    }

    /**
     * Reset counts and text for when changing slides.
     */
    public reset(): void {
      this.current_text = null;
      this.current_text_count = -1;
    }

    /**
     * Check whether there is more text to display or not.
     * Always check this before going to the next slide!!
     */
    public hasMoreText(): boolean {
      return (this.current_text != null && this.current_text_count + 1 in this.current_text);
    }

    /**
     * Get the next block of text for the current slide.
     */
    public setNextText(): void {
      if (this.hasMoreText()) {
        this.setText(this.current_text[++this.current_text_count]);
      }
    }

    public hasPrevText(): boolean {
      return (this.current_text != null && this.current_text_count -1 >= 0);
    }

    /**
     * Set the previous text item for the current slide.
     */
    public setPrevText(): void {
      if (this.hasPrevText()) {
        // Move marker back to item before current.
        this.setText(this.current_text[--this.current_text_count]);
      }
    }

    /**
     * Change word wrap and positioning to fit small dialogue box.
     */
    public smallWindow(): void {
      this.wordWrapWidth = 745;
      this.x = 145;
      this.y = 490;
    }

    /**
     * Change word wrap and positioning to fit large dialogue box.
     */
    public largeWindow(): void {
      this.wordWrapWidth = 830;
      this.x = 35;
      this.y = 470;
    }
  }
}

module Presentation {
  export class Preloader extends Phaser.State {
    private loadingText;
    private static self;

    public preload() {
      Preloader.self = this;
      this.load.onLoadComplete.addOnce(this.startSlideShow, this);
      var style = {font: '65px Arial', fill: '#ffffff', align: 'center'};
      var style2 = {font: '20px Arial', fill: '#ffffff', align: 'center'};
      this.loadingText = this.game.add.text(450, 300, 'Loading...', style);
      var loadingText2 = this.game.add.text(450, 500, 'This may take a few moments on a slow connection.', style2);
      this.loadingText.anchor.set(0.5, 0.5);
      loadingText2.anchor.set(0.5, 0.5);

      // Background music
      this.load.audio('background_music', ['./assets/background_music.mp3']);

      // Webfonts
      //this.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1/webfont.js');

      // Load slide Images.
      this.load.spritesheet('small_host', './assets/host.png', 103, 171, 9);
      this.load.spritesheet('large_host', './assets/large_host.png', 388, 599, 4);
      this.load.spritesheet('dialogue_box_large', './assets/dialogue-box-large.png', 850, 140, 1);
      this.load.spritesheet('dialogue_box_small', './assets/dialogue-box-small.png', 765, 110, 1);
      this.preloadSlideImages('slides');
      this.preloadTemplateImages('slide_templates');
    }

    /**
     * Preload slide images
     */
    private preloadSlideImages(key: string): void {
      var i: number = 0, json: JSON;

      json = JSON.parse(this.game.cache.getText(key));

      Main.slideManager.setSlidesJson(json);
      Main.slideManager.getAllImages(function(imgList) {
        if (imgList != null) {
          for (i; i < imgList.length; i++) {
            // Add all of the images from the slides to be preloaded.
            Preloader.self.load.image(imgList[i], imgList[i]);
          }
        }
      });
    }

    /**
     * Get an array of images from template json file.
     *
     * @param json
     * @param callback
     */
    private static getTemplateImages(json: JSON, callback: Function): void {
      var size: number = 0, key: any, imgList: Array<string> = [];

      // Loop through all of the slides and get images to preload.
      for (key in json) {
        if (json.hasOwnProperty(key)) {
          if ("background_img" in json[key] && json[key].background_img != "") {
            // Add image to list.
            imgList.push(json[key].background_img);
          }
        }
      }

      if (imgList !== undefined) {
        callback(imgList);
      } else {
        callback(null);
      }
    }

    /**
     * Preload all images from templates json file.
     * @param key
     */
    private preloadTemplateImages(key: string): void {
      var json = JSON.parse(this.game.cache.getText(key));

      Preloader.getTemplateImages(json, function(imgList) {
        var i: number = 0;

        if (imgList != null) {
          for (i; i < imgList.length; i++) {
            // Add all of the images from the slides to be preloaded.
            Preloader.self.load.image(imgList[i], imgList[i]);
          }
        }
      });
    }

    /**
     * Start the DigitalGuide state.
     */
    private startSlideShow() {
      // start background music
      var bg_music = this.game.add.audio('background_music');
      bg_music.volume = 0.4;
      bg_music.loop = true;
      bg_music.play();

      this.game.state.start('DigitalGuide');
    }
  }
}

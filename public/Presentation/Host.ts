module Presentation {
  export class SmallHost extends Phaser.Sprite {
    constructor(game: Phaser.Game, x: number, y: number, sprite_sheet: string, frame: number) {
      super(game, x, y, sprite_sheet, frame);

      this.anchor.setTo(0.5, 0.5);

      this.animations.add('normal_talk',
        [0,3,6,7,4,5,0,0,3,6,3,0,3,6,3,0,3,4,5,8,7,4,3], 7, true);

      this.game.add.existing(this);
    }

    /**
     * Change/play an animation sequence.
     *
     * @param key animation key.
     */
    public play_animation(key: string) {
      this.animations.play(key);
    }
  }

  export class LargeHost extends Phaser.Sprite {
    private static self;

    constructor(game: Phaser.Game, x: number, y: number, sprite_sheet: string, frame: number) {
      super(game, x, y, sprite_sheet, frame);
      LargeHost.self = this;
      this.anchor.setTo(0.5, 0.5);

      this.animations.add('normal_talk', [0,1], 7, true);

      this.animations.add('other_talk', [2,3], 7, true);

      this.animations.add('silent', [0], 0, false);

      this.game.add.existing(this);
    }

    /**
     * Change/play an animation sequence.
     *
     * @param key animation key.
     */
    public play_animation(key: string) {
      //this.animations.frame = 0;
      this.animations.play(key);

      if (key == 'normal_talk') {
        var self = this;

        // Halp
        //setTimeout(function(self) {
        //  LargeHost.self.play_animation('silent');
        //}, 4000);
      }
    }
  }
}

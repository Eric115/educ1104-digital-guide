module Presentation {
  export class SlideManager {
    private _slides: JSON;
    private _currentSlide: number;

    get currentSlideId(): number {
      return this._currentSlide;
    }

    set currentSlideId(id: number) {
      this._currentSlide = id;
    }

    public setSlidesJson(slides: JSON): void {
      this._slides = slides;

      // Set current slide default value.
      this._currentSlide = 1;
    }

    /**
     *  Get a list of all images from slides json.
     * @param callback callback function
     * @returns list of images to callbackfunction
     */
    public getAllImages(callback: Function): void {
      var size: number = 0, key: any, imgList: Array<string> = [];

      // Loop through all of the slides and get images to preload.
      for (key in this._slides) {
        if (this._slides.hasOwnProperty(key)) {
          if ("main_img" in this._slides[key] && this._slides[key].main_img != null) {
            // Add image to list.
            imgList.push(this._slides[key].main_img);
          }
        }
      }

      if (imgList !== undefined) {
        callback(imgList);
      } else {
        callback(null);
      }
    }

    /**
     * Get the current slide object.
     *
     * @returns {any}
     */
    public getCurrentSlide(): Slide {
      return this._slides[this._currentSlide];
    }

    /**
     * Get the next slide.
     *
     * @returns {any} or 0 if at last slide.
     */
    public getNextSlide(): Slide {
      if ((this._currentSlide + 1) in this._slides) {
        return this._slides[++this._currentSlide];
      } else {
        return null;
      }
    }

    public getPrevSlide(): Slide {
      if ((this._currentSlide - 1) in this._slides) {
        return this._slides[--this._currentSlide];
      } else {
        return null;
      }
    }
  }
}

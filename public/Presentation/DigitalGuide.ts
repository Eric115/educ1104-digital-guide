module Presentation {
  export class DigitalGuide extends Phaser.State {
    private static self;
    private _slideManager: SlideManager;
    private _currentSlide: Phaser.Image;
    private _smallHost: SmallHost;
    private _largeHost: LargeHost;
    private _dialogueBoxLarge: DialogueBoxLarge;
    private _dialogueBoxSmall: DialogueBoxSmall;
    private static _hostSpeech: HostSpeech;
    private templates: JSON;

    constructor() {
      super();
      DigitalGuide.self = this;
    }

    /**
     * Begin rendering the slideshow.
     */
    public create() {
      // Internal references
      this._slideManager = Main.slideManager;
      this._smallHost = new SmallHost(this.game, 0, 0, 'small_host', 0);
      this._largeHost = new LargeHost(this.game, 0, 0, 'large_host', 0);
      this._dialogueBoxLarge = new DialogueBoxLarge(this.game);
      this._dialogueBoxSmall = new DialogueBoxSmall(this.game);
      DigitalGuide.self._hostSpeech = new HostSpeech(this.game);

      this.clearSlide();

      this.templates = JSON.parse(this.game.cache.getText('slide_templates'));

      // Get first slide
      this.setSlide(this._slideManager.getCurrentSlide(), 2);
    }

    /**
     * Hide all of the main assets, ready for a slide change.
     */
    private clearSlide(): void {
      this._smallHost.visible = false;
      this._largeHost.visible = false;
      this._dialogueBoxLarge.visible = false;
      DigitalGuide.self._hostSpeech.visible = false;
      // Reset text.
      DigitalGuide.self._hostSpeech.reset();
    }

    /**
     * Set the visible slide.
     *
     * @param slide slide object
     * @param transDir 1 for right (default), -1 for left, 2 for none.
     */
    private setSlide(slide: Slide, transDir?: number): void {
      var template: Template = this.templates[slide.template];
      transDir = transDir || 1;

      // Clear elements ready for slide change.
      this.clearSlide();

      // Template setup.
      if (template.background_img != null) {
        var back = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, template.background_img);
        back.anchor.setTo(0.5, 0.5);
        back.z = 1;
      }

      // If the slide has a main img, add it in.
      if ("main_img" in slide && slide.main_img != null) {
        var old_slide:Phaser.Image = this._currentSlide || null,
          start_x = (transDir > 0) ? (transDir === 2) ? template.main_img_origin[0] : 1800 : -900;

        this._currentSlide = this.game.add.image(start_x, template.main_img_origin[1], slide.main_img);
        this._currentSlide.anchor.setTo(0.5, 0.5);

        var transition = this.add.tween(this._currentSlide);
        transition.to({x: template.main_img_origin[0]}, 500, Phaser.Easing.Linear.None, true, 0);
        if (old_slide !== null) {
          transition.onComplete.addOnce(function () {
            // remove old_slide to prevent memory leaks.
            old_slide.destroy();
          }, this);
        }
      } else {
        if (this._currentSlide != null) {
          this._currentSlide.destroy();
        }
        this._currentSlide = null;
      }

      if ("show_host" in template && template.show_host === true) {
        if (template.use_large_host === true) {
          this._largeHost.anchor.setTo(template.host_anchor[0], template.host_anchor[1]);
          this._largeHost.position.setTo(template.host_origin[0], template.host_origin[1]);
          this._largeHost.play_animation(slide.host_animation);
          this._largeHost.visible = true;
          this._largeHost.bringToTop();

          this._largeHost.alpha = 0;
          this.game.add.tween(this._largeHost).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true, 0);

          // If there is speech, render dialogue box.
          if ("host_speech" in slide && slide.host_speech != "") {
            this._dialogueBoxLarge.play_animation('normal');
            this._dialogueBoxLarge.visible = true;
            this._dialogueBoxLarge.bringToTop();

            DigitalGuide.self._hostSpeech.setCurrentSpeech(slide.host_speech, 2);
            DigitalGuide.self._hostSpeech.largeWindow();
            DigitalGuide.self._hostSpeech.visible = true;
            DigitalGuide.self._hostSpeech.bringToTop();
          }
        } else {
          // Use small host
          this._smallHost.anchor.setTo(template.host_anchor[0], template.host_anchor[1]);
          this._smallHost.position.setTo(template.host_origin[0], template.host_origin[1]);
          this._smallHost.play_animation(slide.host_animation);
          this._smallHost.visible = true;
          this._smallHost.bringToTop();

          this._smallHost.alpha = 0;
          this.game.add.tween(this._smallHost).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true, 0);

          if ("host_speech" in slide && slide.host_speech != "") {
            this._dialogueBoxSmall.play_animation('normal');
            this._dialogueBoxSmall.visible = true;
            this._dialogueBoxSmall.bringToTop();

            DigitalGuide.self._hostSpeech.setCurrentSpeech(slide.host_speech, 1);
            DigitalGuide.self._hostSpeech.smallWindow();
            DigitalGuide.self._hostSpeech.visible = true;
            DigitalGuide.self._hostSpeech.bringToTop();
          }
        }
      }
    }

    public static next(): void {
      // Check for more text.
      if (DigitalGuide.self._hostSpeech.hasMoreText()) {
        DigitalGuide.self._hostSpeech.setNextText();
      } else {
        var next: Slide = DigitalGuide.self._slideManager.getNextSlide();
        if (next !== null) {
          DigitalGuide.self.setSlide(next);
        }
      }
    }

    public static previous(): void {
      if (DigitalGuide.self._hostSpeech.hasPrevText()) {
        DigitalGuide.self._hostSpeech.setPrevText();
      } else {
        var prev: Slide = DigitalGuide.self._slideManager.getPrevSlide();
        if (prev !== null) {
          DigitalGuide.self.setSlide(prev, -1);
        }
      }
    }
  }
}

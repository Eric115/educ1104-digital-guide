module Presentation {
  export class Server {
    private static self;
    private static socketID: string;
    private parent;
    private socket;

    constructor(parent) {
      this.parent = parent;
      Server.self = this;

      this.socket = io.connect('http://128.199.78.202:3001');
      //this.socket = io.connect('http://127.0.0.1:3001');

      // Let the server know this is a desktop.
      this.socket.emit('identify', 'desktop');

      this.socket.on('clientId', function(id) {
        Server.socketID = id;
      });

      this.socket.on('connectionCode', function(data) {
        Main.connectionCode = data;
      });

      this.socket.on('next', function(data) {
        parent.next();
      });

      this.socket.on('prev', function(data) {
        parent.previous();
      });
    }
  }
}

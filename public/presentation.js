var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Presentation;
(function (Presentation) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.load.text('slides', './assets/slides.json');
            this.load.text('slide_templates', './assets/templates.json');
        };
        Boot.prototype.create = function () {
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;
            this.game.scale.pageAlignHorizontally = true;
            this.game.state.start('Preloader', true, false);
        };
        return Boot;
    })(Phaser.State);
    Presentation.Boot = Boot;
})(Presentation || (Presentation = {}));
var Presentation;
(function (Presentation) {
    var DialogueBoxLarge = (function (_super) {
        __extends(DialogueBoxLarge, _super);
        function DialogueBoxLarge(game) {
            _super.call(this, game, 25, 600, 'dialogue_box_large', 0);
            this.animations.add('normal', [0], 0, false);
            this.anchor.setTo(0, 1);
            this.visible = true;
            this.game.add.existing(this);
        }
        DialogueBoxLarge.prototype.play_animation = function (key) {
            this.animations.play(key);
        };
        return DialogueBoxLarge;
    })(Phaser.Sprite);
    Presentation.DialogueBoxLarge = DialogueBoxLarge;
    var DialogueBoxSmall = (function (_super) {
        __extends(DialogueBoxSmall, _super);
        function DialogueBoxSmall(game) {
            _super.call(this, game, 890, 590, 'dialogue_box_small', 0);
            this.animations.add('normal', [0], 0, false);
            this.anchor.setTo(1, 1);
            this.visible = true;
            this.game.add.existing(this);
        }
        DialogueBoxSmall.prototype.play_animation = function (key) {
            this.animations.play(key);
        };
        return DialogueBoxSmall;
    })(Phaser.Sprite);
    Presentation.DialogueBoxSmall = DialogueBoxSmall;
})(Presentation || (Presentation = {}));
var Presentation;
(function (Presentation) {
    var DigitalGuide = (function (_super) {
        __extends(DigitalGuide, _super);
        function DigitalGuide() {
            _super.call(this);
            DigitalGuide.self = this;
        }
        DigitalGuide.prototype.create = function () {
            this._slideManager = Presentation.Main.slideManager;
            this._smallHost = new Presentation.SmallHost(this.game, 0, 0, 'small_host', 0);
            this._largeHost = new Presentation.LargeHost(this.game, 0, 0, 'large_host', 0);
            this._dialogueBoxLarge = new Presentation.DialogueBoxLarge(this.game);
            this._dialogueBoxSmall = new Presentation.DialogueBoxSmall(this.game);
            DigitalGuide.self._hostSpeech = new Presentation.HostSpeech(this.game);
            this.clearSlide();
            this.templates = JSON.parse(this.game.cache.getText('slide_templates'));
            this.setSlide(this._slideManager.getCurrentSlide(), 2);
        };
        DigitalGuide.prototype.clearSlide = function () {
            this._smallHost.visible = false;
            this._largeHost.visible = false;
            this._dialogueBoxLarge.visible = false;
            DigitalGuide.self._hostSpeech.visible = false;
            DigitalGuide.self._hostSpeech.reset();
        };
        DigitalGuide.prototype.setSlide = function (slide, transDir) {
            var template = this.templates[slide.template];
            transDir = transDir || 1;
            this.clearSlide();
            if (template.background_img != null) {
                var back = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, template.background_img);
                back.anchor.setTo(0.5, 0.5);
                back.z = 1;
            }
            if ("main_img" in slide && slide.main_img != null) {
                var old_slide = this._currentSlide || null, start_x = (transDir > 0) ? (transDir === 2) ? template.main_img_origin[0] : 1800 : -900;
                this._currentSlide = this.game.add.image(start_x, template.main_img_origin[1], slide.main_img);
                this._currentSlide.anchor.setTo(0.5, 0.5);
                var transition = this.add.tween(this._currentSlide);
                transition.to({ x: template.main_img_origin[0] }, 500, Phaser.Easing.Linear.None, true, 0);
                if (old_slide !== null) {
                    transition.onComplete.addOnce(function () {
                        old_slide.destroy();
                    }, this);
                }
            }
            else {
                if (this._currentSlide != null) {
                    this._currentSlide.destroy();
                }
                this._currentSlide = null;
            }
            if ("show_host" in template && template.show_host === true) {
                if (template.use_large_host === true) {
                    this._largeHost.anchor.setTo(template.host_anchor[0], template.host_anchor[1]);
                    this._largeHost.position.setTo(template.host_origin[0], template.host_origin[1]);
                    this._largeHost.play_animation(slide.host_animation);
                    this._largeHost.visible = true;
                    this._largeHost.bringToTop();
                    this._largeHost.alpha = 0;
                    this.game.add.tween(this._largeHost).to({ alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 0);
                    if ("host_speech" in slide && slide.host_speech != "") {
                        this._dialogueBoxLarge.play_animation('normal');
                        this._dialogueBoxLarge.visible = true;
                        this._dialogueBoxLarge.bringToTop();
                        DigitalGuide.self._hostSpeech.setCurrentSpeech(slide.host_speech, 2);
                        DigitalGuide.self._hostSpeech.largeWindow();
                        DigitalGuide.self._hostSpeech.visible = true;
                        DigitalGuide.self._hostSpeech.bringToTop();
                    }
                }
                else {
                    this._smallHost.anchor.setTo(template.host_anchor[0], template.host_anchor[1]);
                    this._smallHost.position.setTo(template.host_origin[0], template.host_origin[1]);
                    this._smallHost.play_animation(slide.host_animation);
                    this._smallHost.visible = true;
                    this._smallHost.bringToTop();
                    this._smallHost.alpha = 0;
                    this.game.add.tween(this._smallHost).to({ alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 0);
                    if ("host_speech" in slide && slide.host_speech != "") {
                        this._dialogueBoxSmall.play_animation('normal');
                        this._dialogueBoxSmall.visible = true;
                        this._dialogueBoxSmall.bringToTop();
                        DigitalGuide.self._hostSpeech.setCurrentSpeech(slide.host_speech, 1);
                        DigitalGuide.self._hostSpeech.smallWindow();
                        DigitalGuide.self._hostSpeech.visible = true;
                        DigitalGuide.self._hostSpeech.bringToTop();
                    }
                }
            }
        };
        DigitalGuide.next = function () {
            if (DigitalGuide.self._hostSpeech.hasMoreText()) {
                DigitalGuide.self._hostSpeech.setNextText();
            }
            else {
                var next = DigitalGuide.self._slideManager.getNextSlide();
                if (next !== null) {
                    DigitalGuide.self.setSlide(next);
                }
            }
        };
        DigitalGuide.previous = function () {
            if (DigitalGuide.self._hostSpeech.hasPrevText()) {
                DigitalGuide.self._hostSpeech.setPrevText();
            }
            else {
                var prev = DigitalGuide.self._slideManager.getPrevSlide();
                if (prev !== null) {
                    DigitalGuide.self.setSlide(prev, -1);
                }
            }
        };
        return DigitalGuide;
    })(Phaser.State);
    Presentation.DigitalGuide = DigitalGuide;
})(Presentation || (Presentation = {}));
var Presentation;
(function (Presentation) {
    var SmallHost = (function (_super) {
        __extends(SmallHost, _super);
        function SmallHost(game, x, y, sprite_sheet, frame) {
            _super.call(this, game, x, y, sprite_sheet, frame);
            this.anchor.setTo(0.5, 0.5);
            this.animations.add('normal_talk', [0, 3, 6, 7, 4, 5, 0, 0, 3, 6, 3, 0, 3, 6, 3, 0, 3, 4, 5, 8, 7, 4, 3], 7, true);
            this.game.add.existing(this);
        }
        SmallHost.prototype.play_animation = function (key) {
            this.animations.play(key);
        };
        return SmallHost;
    })(Phaser.Sprite);
    Presentation.SmallHost = SmallHost;
    var LargeHost = (function (_super) {
        __extends(LargeHost, _super);
        function LargeHost(game, x, y, sprite_sheet, frame) {
            _super.call(this, game, x, y, sprite_sheet, frame);
            LargeHost.self = this;
            this.anchor.setTo(0.5, 0.5);
            this.animations.add('normal_talk', [0, 1], 7, true);
            this.animations.add('other_talk', [2, 3], 7, true);
            this.animations.add('silent', [0], 0, false);
            this.game.add.existing(this);
        }
        LargeHost.prototype.play_animation = function (key) {
            this.animations.play(key);
            if (key == 'normal_talk') {
                var self = this;
            }
        };
        return LargeHost;
    })(Phaser.Sprite);
    Presentation.LargeHost = LargeHost;
})(Presentation || (Presentation = {}));
var Presentation;
(function (Presentation) {
    var HostSpeech = (function (_super) {
        __extends(HostSpeech, _super);
        function HostSpeech(game) {
            _super.call(this, game, 35, 470, '', {
                font: 'Arial',
                fontSize: '18px',
                fill: "#ffffff",
                wordWrap: true,
                wordWrapWidth: 830
            });
            this.current_text = [];
            this.current_text_count = -1;
            this.game.add.existing(this);
        }
        HostSpeech.prototype.setCurrentSpeech = function (text, size) {
            size = size || 1;
            this.current_text_count = -1;
            if (size === 1) {
                this.current_text = text.replace(/.{325}\S*\s+/g, "$&@").split(/\s+@/);
            }
            else if (size == 2) {
                this.current_text = text.replace(/.{480}\S*\s+/g, "$&@").split(/\s+@/);
            }
            this.setNextText();
            this.visible = true;
        };
        HostSpeech.prototype.reset = function () {
            this.current_text = null;
            this.current_text_count = -1;
        };
        HostSpeech.prototype.hasMoreText = function () {
            return (this.current_text != null && this.current_text_count + 1 in this.current_text);
        };
        HostSpeech.prototype.setNextText = function () {
            if (this.hasMoreText()) {
                this.setText(this.current_text[++this.current_text_count]);
            }
        };
        HostSpeech.prototype.hasPrevText = function () {
            return (this.current_text != null && this.current_text_count - 1 >= 0);
        };
        HostSpeech.prototype.setPrevText = function () {
            if (this.hasPrevText()) {
                this.setText(this.current_text[--this.current_text_count]);
            }
        };
        HostSpeech.prototype.smallWindow = function () {
            this.wordWrapWidth = 745;
            this.x = 145;
            this.y = 490;
        };
        HostSpeech.prototype.largeWindow = function () {
            this.wordWrapWidth = 830;
            this.x = 35;
            this.y = 470;
        };
        return HostSpeech;
    })(Phaser.Text);
    Presentation.HostSpeech = HostSpeech;
})(Presentation || (Presentation = {}));
var Presentation;
(function (Presentation) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            Preloader.self = this;
            this.load.onLoadComplete.addOnce(this.startSlideShow, this);
            var style = { font: '65px Arial', fill: '#ffffff', align: 'center' };
            var style2 = { font: '20px Arial', fill: '#ffffff', align: 'center' };
            this.loadingText = this.game.add.text(450, 300, 'Loading...', style);
            var loadingText2 = this.game.add.text(450, 500, 'This may take a few moments on a slow connection.', style2);
            this.loadingText.anchor.set(0.5, 0.5);
            loadingText2.anchor.set(0.5, 0.5);
            this.load.audio('background_music', ['./assets/background_music.mp3']);
            this.load.spritesheet('small_host', './assets/host.png', 103, 171, 9);
            this.load.spritesheet('large_host', './assets/large_host.png', 388, 599, 4);
            this.load.spritesheet('dialogue_box_large', './assets/dialogue-box-large.png', 850, 140, 1);
            this.load.spritesheet('dialogue_box_small', './assets/dialogue-box-small.png', 765, 110, 1);
            this.preloadSlideImages('slides');
            this.preloadTemplateImages('slide_templates');
        };
        Preloader.prototype.preloadSlideImages = function (key) {
            var i = 0, json;
            json = JSON.parse(this.game.cache.getText(key));
            Presentation.Main.slideManager.setSlidesJson(json);
            Presentation.Main.slideManager.getAllImages(function (imgList) {
                if (imgList != null) {
                    for (i; i < imgList.length; i++) {
                        Preloader.self.load.image(imgList[i], imgList[i]);
                    }
                }
            });
        };
        Preloader.getTemplateImages = function (json, callback) {
            var size = 0, key, imgList = [];
            for (key in json) {
                if (json.hasOwnProperty(key)) {
                    if ("background_img" in json[key] && json[key].background_img != "") {
                        imgList.push(json[key].background_img);
                    }
                }
            }
            if (imgList !== undefined) {
                callback(imgList);
            }
            else {
                callback(null);
            }
        };
        Preloader.prototype.preloadTemplateImages = function (key) {
            var json = JSON.parse(this.game.cache.getText(key));
            Preloader.getTemplateImages(json, function (imgList) {
                var i = 0;
                if (imgList != null) {
                    for (i; i < imgList.length; i++) {
                        Preloader.self.load.image(imgList[i], imgList[i]);
                    }
                }
            });
        };
        Preloader.prototype.startSlideShow = function () {
            var bg_music = this.game.add.audio('background_music');
            bg_music.volume = 0.4;
            bg_music.loop = true;
            bg_music.play();
            this.game.state.start('DigitalGuide');
        };
        return Preloader;
    })(Phaser.State);
    Presentation.Preloader = Preloader;
})(Presentation || (Presentation = {}));
/// <reference path="../libs/phaser.d.ts"/>
/// <reference path="../libs/socket.io.d.ts"/>
var Presentation;
(function (Presentation) {
    var Main = (function (_super) {
        __extends(Main, _super);
        function Main() {
            _super.call(this, 900, 600, Phaser.AUTO, 'container', null);
            Main.slideManager = new Presentation.SlideManager();
            this.state.add('Boot', Presentation.Boot, false);
            this.state.add('Preloader', Presentation.Preloader, false);
            this.state.add('DigitalGuide', Presentation.DigitalGuide, false);
            this.server = new Presentation.Server(this);
            this.state.start('Boot');
        }
        Main.prototype.next = function () {
            Presentation.DigitalGuide.next();
        };
        Main.prototype.previous = function () {
            Presentation.DigitalGuide.previous();
        };
        return Main;
    })(Phaser.Game);
    Presentation.Main = Main;
})(Presentation || (Presentation = {}));
var Presentation;
(function (Presentation) {
    var Server = (function () {
        function Server(parent) {
            this.parent = parent;
            Server.self = this;
            this.socket = io.connect('http://128.199.78.202:3001');
            this.socket.emit('identify', 'desktop');
            this.socket.on('clientId', function (id) {
                Server.socketID = id;
            });
            this.socket.on('connectionCode', function (data) {
                Presentation.Main.connectionCode = data;
            });
            this.socket.on('next', function (data) {
                parent.next();
            });
            this.socket.on('prev', function (data) {
                parent.previous();
            });
        }
        return Server;
    })();
    Presentation.Server = Server;
})(Presentation || (Presentation = {}));
var Presentation;
(function (Presentation) {
    var SlideManager = (function () {
        function SlideManager() {
        }
        Object.defineProperty(SlideManager.prototype, "currentSlideId", {
            get: function () {
                return this._currentSlide;
            },
            set: function (id) {
                this._currentSlide = id;
            },
            enumerable: true,
            configurable: true
        });
        SlideManager.prototype.setSlidesJson = function (slides) {
            this._slides = slides;
            this._currentSlide = 1;
        };
        SlideManager.prototype.getAllImages = function (callback) {
            var size = 0, key, imgList = [];
            for (key in this._slides) {
                if (this._slides.hasOwnProperty(key)) {
                    if ("main_img" in this._slides[key] && this._slides[key].main_img != null) {
                        imgList.push(this._slides[key].main_img);
                    }
                }
            }
            if (imgList !== undefined) {
                callback(imgList);
            }
            else {
                callback(null);
            }
        };
        SlideManager.prototype.getCurrentSlide = function () {
            return this._slides[this._currentSlide];
        };
        SlideManager.prototype.getNextSlide = function () {
            if ((this._currentSlide + 1) in this._slides) {
                return this._slides[++this._currentSlide];
            }
            else {
                return null;
            }
        };
        SlideManager.prototype.getPrevSlide = function () {
            if ((this._currentSlide - 1) in this._slides) {
                return this._slides[--this._currentSlide];
            }
            else {
                return null;
            }
        };
        return SlideManager;
    })();
    Presentation.SlideManager = SlideManager;
})(Presentation || (Presentation = {}));

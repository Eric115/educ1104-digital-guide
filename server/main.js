/// <reference path="lib/Node.d.ts"/>
/// <reference path="lib/socket.io.d.ts"/>
var Server = (function () {
    function Server() {
        this.io = require("socket.io")();
        Server.self = this;
        Server.clients = {};
        this.listen();
    }
    Server.identifyClient = function (type) {
        var id = this.id;
        Server.clients[id].type = type;
        Server.log(this.id + ' is ' + type);
    };
    Server.log = function (msg) {
        Server.util.log(msg);
    };
    Server.prototype.listen = function () {
        var self = this;
        this.io.on('connection', function (client) {
            Server.log('New client - ' + client.id);
            Server.clients[client.id] = client;
            client.emit('clientId', client.id);
            client.on('identify', Server.identifyClient);
            client.on('next', Server.self.emitNext);
            client.on('prev', Server.self.emitPrev);
        });
        this.io.listen(3001);
        console.log('server listening...');
    };
    Server.prototype.emitNext = function () {
        Server.log('Next!');
        this.broadcast.emit('next');
    };
    Server.prototype.emitPrev = function () {
        Server.log('Prev!');
        this.broadcast.emit('prev');
    };
    Server.util = require("util");
    return Server;
})();
var server = new Server();

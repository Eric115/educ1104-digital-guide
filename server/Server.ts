/// <reference path="lib/Node.d.ts"/>
/// <reference path="lib/socket.io.d.ts"/>

interface Connection {
  desktop_id: string;
  mobile_id?: string;
  connection_code: number;
}

class Server {
  private io = require("socket.io")();
  private static clients: any;
  private static connectionList: Connection;
  public static util = require("util");
  public static self;

  constructor() {
    Server.self = this;
    Server.clients = {};
    this.listen();
  }

  public static identifyClient(type): void {
    var id: string = this.id;
    Server.clients[id].type = type;
    Server.log(this.id + ' is ' + type);
  }

  public static log(msg: string): void {
    Server.util.log(msg);
  }

  private listen(): void {
    var self = this;
    this.io.on('connection', function(client) {
      Server.log('New client - ' + client.id);
      // Check this is a new client.
      Server.clients[client.id] = client;
      client.emit('clientId', client.id);
      client.on('identify', Server.identifyClient);
      client.on('next', Server.self.emitNext);
      client.on('prev', Server.self.emitPrev);
    });
    this.io.listen(3001);
    console.log('server listening...');
  }

  public emitNext(): void {
    Server.log('Next!');
    this.broadcast.emit('next');
  }

  public emitPrev(): void {
    Server.log('Prev!');
    this.broadcast.emit('prev');
  }
}

var server = new Server();
